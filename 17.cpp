#include <iostream>
#include <cmath>

class Vector
{
public:
    Vector() : x(0) ,y(0) ,z(0)
    {}
    Vector(double _x,double _y, double _z) : x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        std::cout << x << " " << y << " " << z << '\n';
    }
    void absolute()
    {
        double abs = pow(pow(x, 2) + pow(y, 2) + pow(z, 2), 0.5);
        std::cout << "Vector absolute: " << abs << '\n';
    }
private:
    double x;
    double y;
    double z;
};

class Human
{
private:
    int HP;
    int Stamina;
    int Mana;
public:
    Human() : HP(100), Stamina(100), Mana(100)
    {}
    Human(int _HP,int _Stamina,int _Mana) : HP(_HP), Stamina(_Stamina), Mana(_Mana)
    {}
    void ShowStats()
    {
        std::cout << "HP: " << HP << " " <<  "Stamina: " << Stamina << " " << "Mana: " << Mana << '\n';
    }
};

int main()
{
    Human Player(49,74,8);
    Player.ShowStats();
    Vector v(1, 1, 1);
    v.absolute();
}